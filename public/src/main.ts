import * as THREE from "three";
import { StateMachine, ITransition, tFrom } from "./FSM/stateMachine";
// import {FiniteStateMachine} from "typestate";

declare function require(name:string);

import { ARMLContext } from "./ARClasses/ARML";
import { Geometry } from "./ARClasses/Geometry";
import { Feature } from "./ARClasses/Feature";
import { VisualAsset } from "./ARClasses/VisualAsset";
import { ARAnchor } from "./ARClasses/ARAnchor";

import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { VRButton } from 'three/examples/jsm/webxr/VRButton';
import { XRControllerModel, XRControllerModelFactory } from 'three/examples/jsm/webxr/XRControllerModelFactory'
const TWEEN = require('es6-tween');

import { Region } from "./ARClasses/Region"
import { UseableFace } from "./ARClasses/UseableFace";
import { WindowPlacementManager } from "./WindowPlacementManager";
import { RegionManager } from "./RegionManager";
// import { createBrotliDecompress } from "zlib";
import { ViewRectangleTracker } from "./ARClasses/ViewRectangleTracker";
import { Image } from "./ARClasses/Image";
import { Model } from "./ARClasses/Model";
import { Icon } from "./ARClasses/Icon";
import { MainMenu } from "./ARClasses/MainMenu";
import { MenuRegion } from "./ARClasses/MenuRegion";
import { BufferAttribute } from "three";

declare function create(o: object | null): void;

let cameraBox : THREE.Group;
let camera : THREE.PerspectiveCamera;
let scene : THREE.Scene;
let renderer : THREE.WebGLRenderer;
let controls : any;
let mainMenu : MainMenu;
let house : any;
let wallGroup : THREE.Object3D;
// let controlFSM : StateMachine<UserActionStates, UserActionEvents>;
// let gazeFSM : StateMachine<GazeStates, GazeEvents>;

enum UserActionStates { Browse = 0,RegionPickedUpClick, RegionPickedUpDrag, OtherAction, DropObject };
enum UserActionEvents { DragClick = 50, Drag, OtherClick, DragEnd, Reset };

enum GazeStates { Nothing = 100, LookingAtObject };
enum GazeEvents { LookingAtNothing = 150, LookingAtObject, LookingAtDifferentObject };

let yOffset : number = 0;//-10;

let objects : any[] = [];
let usableFaces : UseableFace[] = []
let walls : any[] = [];

let raycaster : THREE.Raycaster;
let raycastTargetScene : THREE.Scene;
let intersections : THREE.Intersection[];
let onObject : boolean;
let currObject : any; // Current object in raycaster sight
let currRegion : Region | undefined; // currObject's region
let currParent : any;

let contentContext : ARMLContext[];
let crosshair : any;

let moveForward : boolean = false;
let moveBackward : boolean = false;
let moveLeft : boolean = false;
let moveRight : boolean = false;
let canJump : boolean = false;

let clickDistance : number = 0.5;
let lastClickedPoint = new THREE.Vector3();
let isClick = false;

let prevTime = performance.now();
let velocity = new THREE.Vector3();
let direction = new THREE.Vector3();
let vertex : THREE.Vector3 = new THREE.Vector3();
let color : THREE.Color = new THREE.Color();
let helmet = null;

let cameraPosition : THREE.Vector3 = new THREE.Vector3(0, 0, 0);
let cameraDirection : THREE.Vector3 = new THREE.Vector3(0, 0, 0);
// let arrow : THREE.ArrowHelper;
let prevObject : any;
let objectInHand : boolean = false;
let pickedObject : Region = undefined;

let verticalPlaneSegments = 1;
let maxSegmentSize = 35;

let controller1 : THREE.Group;
let controller2 : THREE.Group;
let controllerGrip1 : THREE.Group;
let controllerGrip2 : THREE.Group;

// If mouse clicked down
let clickedDown : boolean = false;

// If staring directly at feed
let staringAtFeed : boolean = false;

//Controller teleport
let tempVecP : THREE.Vector3 = new THREE.Vector3();
let tempVecV : THREE.Vector3 = new THREE.Vector3();
let tempVec : THREE.Vector3 = new THREE.Vector3();
const lineSegments = 10;
const lineGeometry = new THREE.BufferGeometry();
const lineGeometryVertices = new Float32Array((lineSegments + 1) * 3);

lineGeometryVertices.fill(0);

lineGeometry.setAttribute('position', new THREE.BufferAttribute(lineGeometryVertices, 3));

const lineMaterial : THREE.LineBasicMaterial = new THREE.LineBasicMaterial({ color: 0x888888, blending: THREE.AdditiveBlending });

let guideline : THREE.Line = new THREE.Line( lineGeometry, lineMaterial );
let g = new THREE.Vector3(0, -9.8, 0);


init();
animate();

function init() {

    scene = new THREE.Scene();
	raycastTargetScene = new THREE.Scene();
	scene.add(raycastTargetScene);
    console.log("Scene", scene);


    raycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3() );
    // raycaster.linePrecision = 0.1;

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.shadowMap.enabled = true;
	renderer.xr.enabled = true;
    document.body.appendChild( renderer.domElement );

    window.addEventListener( 'resize', onWindowResize, false );
    document.body.appendChild( VRButton.createButton( renderer ) );


    let background : THREE.CubeTexture = createWorld(scene);
    addPointerLockControls();

    // createFSMs();
    // controllers

    controller1 = renderer.xr.getController( 0 );
    scene.add( controller1 );

    controller2 = renderer.xr.getController( 1 );
    scene.add( controller2 );

    var controllerModelFactory = new XRControllerModelFactory();

    controllerGrip1 = renderer.xr.getControllerGrip( 0 );
    controllerGrip1.add( controllerModelFactory.createControllerModel( controllerGrip1 ) );
    scene.add( controllerGrip1 );

    controllerGrip2 = renderer.xr.getControllerGrip( 1 );
    controllerGrip2.add( controllerModelFactory.createControllerModel( controllerGrip2 ) );
    scene.add( controllerGrip2 );



    controller1.addEventListener( 'selectstart', onSelectStart );
    controller1.addEventListener( 'selectend', onSelectEnd );
    controller2.addEventListener( 'selectstart', onSelectStart );
    controller2.addEventListener( 'selectend', onSelectEnd );
    
    controller1.add(guideline);

    // var geometry = new THREE.BufferGeometry().setFromPoints( [ new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, - 1 ) ] );

    // var line = new THREE.Line( geometry );
    // line.name = 'line';
    // line.scale.z = 1000;

    // controller1.add( line.clone() );
    // controller2.add( line.clone() );

    let arml = createARMLContext(scene, "sdf");

    //requestAnimationFrame( animate );
    // If VR 
    renderer.setAnimationLoop( animate );
}


function sceneLoaded() {
    RegionManager.createAllRegions();
    RegionManager.setRaycastScene(raycastTargetScene);
    let coverage = document.getElementById( 'info2' );
    (document.getElementById( 'instructions' ).style.opacity as unknown) = 1.0;
    coverage.parentNode.removeChild(coverage);

    // Regions will align after all the regions are registered with RegionManager
    WindowPlacementManager.alignWithMenu();

    house.position.y += yOffset;
}


function createCrosshair(camera : THREE.Camera, filePath : string) {
    let imageLoader : THREE.TextureLoader = new THREE.TextureLoader();
    let imageMaterial : any = new THREE.MeshBasicMaterial({
        map: imageLoader.load( filePath ),
        transparent: true
        //        side: THREE.DoubleSide
    });
    let imageGeometry : THREE.PlaneBufferGeometry = new THREE.PlaneBufferGeometry( 0.5, 0.25 );
    let crosshairMesh : THREE.Mesh = new THREE.Mesh(imageGeometry, imageMaterial);
    crosshairMesh.position.set(0, 0, -1 / 7);
    crosshairMesh.scale.set(0.1 / 7, 0.1 / 7, 0.1 / 7);
    crosshairMesh.lookAt(new THREE.Vector3(0, 0, 0));
    return crosshairMesh;
}

function createARMLContext(scene : THREE.Scene, xml_input : string) {
    let arml : ARMLContext = new ARMLContext("helmet");
    // Add all interactive objects in a different scene for fast searching
    createHelmet(arml, raycastTargetScene);
    createTodoList(arml, raycastTargetScene);
    arml.addContextToScene(raycastTargetScene);
    return arml;
}


function createHelmet(arml : ARMLContext, scene : THREE.Scene) {
    let helmetAsset = new Model('517 Garbage Bin.gltf', {filePath: 'models/gltf/trashbin/', background: scene.background, scale : new THREE.Vector3(0.05 / 7, 0.05 / 7, 0.05 / 7)});
    helmet = helmetAsset;
    console.log(helmet);
    /* let labelAsset = new Label(' Helmet ',
                { fontsize: 32, fontface: "Georgia", borderColor: {r: 0, g: 0, b: 255, a: 1.0}});
    let labelAnchor = new Geometry(Geometry.TYPE.Point, [new THREE.Vector3(5, 15, -15)], [labelAsset]); */
    let helmetAnchor = new Geometry(Geometry.TYPE.Point, [new THREE.Vector3(30 / 7, 9.5 / 7, -15.4 / 7)], [helmetAsset], true);
    //let helmetAnchor = new ViewRectangleTracker(ViewRectangleTracker.TYPE.ViewRectangleTrackerLeft, 3, "Social", [helmetAsset]);
    //helmetAnchor.orient = false;
    let helmetFeature = new Feature("Helmet", "Cool spinning helmet", true, "Watevs", [helmetAnchor]);//, labelAnchor]);
    arml.addFeatures([helmetFeature]);
}

function createTodoList(arml : ARMLContext, scene : THREE.Scene) {
    let instaAsset : Image = new Image(scene, './images/instagram.png', 6 / 7, './images/ig_glance.png', "http://instagram.com");
    // let facebookAsset : Image = new Image(scene, './images/fb.png', 5, './images/fb_expanded.png', "facebook.com");
    let twitterAsset : Image = new Image(scene, './images/small_twitter.png', 6 / 7, './images/small_twitter_expanded.png', "http://twitter.com");
    // let tempRegion : Region = new Region(scene, 1, "Unlisted", [instaAsset, twitterAsset]);
    
    let controlsAsset : Image = new Image(scene, './images/controls.png', 9 / 7, './images/controls_expanded.png');
    let agendaAsset : Image = new Image(scene, './images/agenda_icon.png', 1, './images/agenda_view.png');
    //let todoFeature = new Feature("Todo", "Thingy", true, "Whatever", [controlsTracker, calendarTracker]);//, twitterTracker, facebookTracker]);
    //let tempRegion2 : Region = new Region(scene, 2, "tv", [controlsAsset]);
    //let tempRegion3 : Region = new Region(scene, 3, "recipe", [controlsAsset]);
    let newsAsset : Image = new Image(scene, './images/news.png', 1, './images/news_expanded.png', 'http://news.google.com');
    let outlookAsset : Image = new Image(scene, './images/outlook.png', 8 / 7, './images/outlook_expanded.png', 'http://outlook.com');
    let slackAsset : Image = new Image(scene, './images/slack.png', 1, './images/slack_expanded.png', 'http://slack.com');
    let tvAsset : Image = new Image(scene, './images/tv.png', 8 / 7);
    let recipeAsset : Image = new Image(scene, './images/recipe_icon.png', 6 / 7, './images/recipe.png');
    RegionManager.addRegionsFromAssets(scene, [controlsAsset, newsAsset, agendaAsset, outlookAsset, slackAsset, instaAsset, twitterAsset]);
    RegionManager.addRegionsFromAssets(scene, [tvAsset], "tv");
    RegionManager.addRegionsFromAssets(scene, [recipeAsset], "recipe");
    // RegionManager.addRegions([tempRegion]);
    //arml.addFeatures([todoFeature]);
}


function createWorld(scene : THREE.Scene) {
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    crosshair = createCrosshair(camera, './images/eye_crosshair.png');
    camera.position.set(0, 1.6, 0);
    cameraBox = new THREE.Group();
    cameraBox.add(camera)
    camera.add(crosshair);
    scene.add(cameraBox);
    //scene.fog = new THREE.Fog( 0xffffff, 0, 750 );

    let light : THREE.HemisphereLight = new THREE.HemisphereLight( 0xeeeeff, 0x777788, 0.75 );
    light.position.set( 0.5, 15, 0.75 );
    scene.add( light );

    loadGLTFModel(scene, "models/gltf/", "model.gltf");
    createHouse( raycastTargetScene );

    let background : THREE.CubeTexture = addSkybox( scene );

    mainMenu = new MainMenu();
    mainMenu.addToScene(scene, camera);
    MenuRegion.attachMenu(mainMenu.localObject);
    raycastTargetScene.add(mainMenu.localObject);
    
    return background;
}

function getRequest( scene : THREE.Scene, url : string ) {
    let xhr = new XMLHttpRequest();
    xhr.open( 'GET', url );
    xhr.onload = function() {
        if ( xhr.status === 200 ) {
            drawHouse( scene, JSON.parse( xhr.responseText ));
        } else {
            console.log( 'Request failed.  Returned status of ' + xhr.status );
        }
    };
    xhr.send();
}

function drawHouse( scene : THREE.Scene, house : object ) {
    wallGroup = new THREE.Object3D();
    house["planes"].forEach( function( value ) {
        createPlane( wallGroup, value );
    });
    scene.add(wallGroup);
    WindowPlacementManager.appendObjects(usableFaces);
}

function loadGLTFModel(scene : THREE.Scene, filePath: string, fileName: string) {
    let gloader = new GLTFLoader();
    gloader.setPath(filePath);
    gloader.load(fileName, (gltf) => {
        scene.add(gltf.scene);
        house = gltf.scene;
        sceneLoaded();
    }, function(xhr) {
        if(xhr.lengthComputable) {
            let percentComplete : number = Math.round(xhr.loaded / xhr.total * 100);
            document.getElementById("progress").innerHTML = `${percentComplete}%`;
        }
    }, function (e) {
        console.error(e);
    } );
}

function createPlane( scene : THREE.Object3D, plane : object ) {
    let scaleDown = 7;
    let material : THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: 0x00ff00, opacity: 0.4, transparent: true, side: THREE.DoubleSide});
    //material.transparent = true;
    let geometry : any = new THREE.PlaneBufferGeometry( plane["size"][0] / scaleDown, plane["size"][1] / scaleDown );
    let wall = new THREE.Mesh(geometry, material);
    wall.position.x = plane["position"][0] / scaleDown;
    wall.position.y = plane["position"][1] / scaleDown + yOffset;
    wall.position.z = plane["position"][2] / scaleDown;
    wall.rotation.x = plane["rotation"][0] / 180 * Math.PI;
    wall.rotation.y = plane["rotation"][1] / 180 * Math.PI;
    wall.rotation.z = plane["rotation"][2] / 180 * Math.PI;
    //console.log(wall);
    scene.add(wall);
    scene.attach(wall);
    //raycastTargetScene.add( wall );
    walls.push(wall);
    wall.updateMatrixWorld(false);
    let [centroids, normals] = extractCentroidsNormalsFromPlane( wall );
    let centroidClone = centroids;//.map(a => new THREE.Vector3(a));
    // console.log(centroidClone);
    for( let j = 0; j < normals.length; j++ ) {
        wall.localToWorld(centroidClone[j]);
        let centroidData : UseableFace = new UseableFace(centroidClone[j], normals[j], wall, plane["category"]);
        usableFaces.push(centroidData);
        wall.userData = {"parent": centroidData};
    }
    wall.visible = false;
    //scene.remove( wall );
}

function createHouse(scene: THREE.Scene) {
    getRequest( scene, './house_faces.json' );
}

function addSkybox(scene: THREE.Scene) : THREE.CubeTexture {
    let urls : string[] = [ 'posx.jpg', 'negx.jpg', 'posy.jpg', 'negy.jpg', 'posz.jpg', 'negz.jpg' ];
    //let urls : string[] = [ 'velcor_rt.jpg', 'velcor_lf.jpg', 'velcor_up.jpg', 'velcor_dn.jpg', 'velcor_bk.jpg', 'velcor_ft.jpg' ];
    let loader : THREE.CubeTextureLoader = new THREE.CubeTextureLoader();
    //loader.setPath('textures/cube/Bridge2/');
    loader.setPath('textures/cube/lake/');
    let background : THREE.CubeTexture = loader.load( urls );
    scene.background = background;
    return background;
}

function toggleWallVisibility() {
    walls.forEach((value) => { value.visible = !value.visible });
}

function extractCentroidsNormalsFromPlane(objectRef : THREE.Mesh) {
    let buffGeom : THREE.BufferGeometry = objectRef.geometry as THREE.BufferGeometry;
    let allPositions : number [] = (buffGeom.attributes.position.array as number[]);
    let position : number [] = new Array<number>();

    let normalArray : number [] = (buffGeom.attributes.normal.array as number[])
    /*let oldNormal = normalArray.filter(function (val, i, arr) {
    // Take the first normal of 3, and check that it's y is 0
        if (i % 9 < 3 && arr[i - (i % 9 - 1)] == 0) {
            if (i % 9 < 2) {
                position.push(allPositions[i]);
            } else {
                for (let j = 0; j < 7; j++) {
                    position.push(allPositions[i + j]);
                }
            }
            return true;
        }
        return false;
    });*/
    //console.log("oldnormal", oldNormal);
    // console.log("allpos", allPositions);
    let centroids : THREE.Vector3[] = []
    if (Math.abs(allPositions[0] * 2) < maxSegmentSize) { // if size < maxSegmentSize, keep one segment
        centroids.push(new THREE.Vector3(0, 0, 0));
    } else {
        let sectionSize : number = Math.abs(allPositions[0] * 2) / verticalPlaneSegments; // divide it into equal segments
        let currentX : number = allPositions[0];
        for(let i = 0; i < verticalPlaneSegments; i++) {
            let centroid = new THREE.Vector3( (2 * currentX + sectionSize) / 2, 0, 0); // (currentX + (currentX + sectionSize)) / 2
            currentX += sectionSize;
            centroids.push(centroid);
        }
    }

    let normal : THREE.Vector3 = new THREE.Vector3(normalArray[0], normalArray[1], normalArray[2]);
    let normalMatrix = new THREE.Matrix3().getNormalMatrix( objectRef.matrixWorld );
    let newNormal = normal.clone().applyMatrix3( normalMatrix ).normalize();
    let normals : THREE.Vector3 [] = [newNormal];
    if (centroids.length > 1) {
        for(let i = 0; i < verticalPlaneSegments - 1; i++) {
            normals.push(newNormal);
        }
    }

    //console.log(newNormal);

    //return combineSurfaces(position, normals);
    return [centroids, normals]
}


function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
}


let SPEED = 0.01;

function rotateObject(object) {
    // object.rotation.x -= SPEED * 2;
    object.rotation.y -= SPEED * 2;
    // object.rotation.z -= SPEED * 3;
}
    /*
function createBlinkingTween() {
    let currScale = 0.5;
    tween = new TWEEN.Tween({scale: 0.75}).to({scale: 1.5}, 1000)
        .easing(TWEEN.Easing.Quadratic.InOut)
        .on('update', ({scale}) => {
            currScale = scale;
            adjustLookedAtObjectScale(currScale);
        })
        .on('complete', () => {adjustLookedAtObjectScale(1)});
    this.reverseTween = new TWEEN.Tween({opacity: 1.0}).to({opacity: 0.0}, 500)
        .on('start', () => {(this.asset.material as THREE.Material).visible = true;})
        .easing(TWEEN.Easing.Quadratic.InOut)
        .on('update', ({opacity}) => {
            let fadeMaterial : THREE.Material = this.fadeAsset.material as THREE.Material;
            let material : THREE.Material = this.asset.material as THREE.Material;
            material.opacity = 1.0 - opacity;
            fadeMaterial.opacity = opacity;
        })
        .on('complete', () => {(this.fadeAsset.material as THREE.Material).visible = false;});   
}

function adjustLookedAtObjectScale(scaleNo : number) {

}*/




function addPointerLockControls() {
    controls = new PointerLockControls( camera, document.body );

    let instructions = document.getElementById( 'instructions' );

    instructions.addEventListener( 'click', function () {
        controls.lock();
    }, false );

    controls.addEventListener( 'lock', function () {
        instructions.style.display = 'none';
    } );

    controls.addEventListener( 'unlock', function () {
        instructions.style.display = '';
    } );

    scene.add( controls.getObject() );

    let onKeyDown = function ( event ) {
        switch ( event.keyCode ) {
            case 38: // up
            case 87: // w
                moveForward = true;
                break;

            case 37: // left
            case 65: // a
                moveLeft = true;
                break;

            case 40: // down
            case 83: // s
                moveBackward = true;
                break;

            case 39: // right
            case 68: // d
                moveRight = true;
                break;

                /* case 32: // space
                if ( canJump === true ) velocity.y += 350;
                canJump = false;
                break; */
        }

    };

    let onKeyUp = function ( event ) {
        switch ( event.keyCode ) {
            case 38: // up
            case 87: // w
                moveForward = false;
                break;

            case 37: // left
            case 65: // a
                moveLeft = false;
                break;

            case 40: // down
            case 83: // s
                moveBackward = false;
                break;

            case 39: // right
            case 68: // d
                moveRight = false;
                break;

            case 84: // t
                toggleWallVisibility();
                break;

            case 77: // m
                mainMenu.toggleMenu(cameraPosition, cameraDirection);
                break;
        }
        if(!objectInHand && !staringAtFeed) {
            WindowPlacementManager.setMarkerPositions(camera);
        }
    };

    document.addEventListener('keydown', onKeyDown, false);
    document.addEventListener('keyup', onKeyUp, false);
}


function onSelectStart() {

    this.userData.isSelecting = true;

}

function onSelectEnd() {

    this.userData.isSelecting = false;

}


function makeControllerTeleport(guidingController) {



    // Controller start position
    let p = guidingController.getWorldPosition(tempVecP);

    // Set Vector V to the direction of the controller, at 1m/s
    let v = guidingController.getWorldDirection(tempVecV);

    // Scale the initial velocity to 6m/s
    v.multiplyScalar(6);

    // Calculate t, this is the above equation written as JS
    const t = (-v.y  + Math.sqrt(v.y**2 - 2*p.y*g.y))/g.y;

    const vertex = tempVec.set(0,0,0);

    for (let i=1; i<=lineSegments; i++) {

        // set vertex to current position of the virtual ball at time t
        positionAtT(vertex,i*t/lineSegments,p,v,g);

        // Copy it to the Array Buffer
        vertex.toArray(lineGeometryVertices,i*3);
    }

    ((guideline.geometry as THREE.BufferGeometry).attributes.position as BufferAttribute).needsUpdate = true;
}

function positionAtT(inVec, t, p, v, g) {

    inVec.copy(p);
    inVec.addScaledVector(v,t);
    inVec.addScaledVector(g,0.5*t**2);
    return inVec;
  }


function animate() {

    if ( controls.isLocked === true ) {

        let time = performance.now();
        let delta = ( time - prevTime ) / 1000;

        velocity.x -= velocity.x * 10.0 * delta;
        velocity.z -= velocity.z * 10.0 * delta;

        // velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

        direction.z = Number( moveForward ) - Number( moveBackward );
        direction.x = Number( moveLeft ) - Number( moveRight );
        direction.normalize(); // this ensures consistent movements in all directions

        if ( moveForward || moveBackward ) velocity.z -= direction.z * 400.0 * delta;
        if ( moveLeft || moveRight ) velocity.x -= direction.x * 400.0 * delta;

        /* if ( onObject === true ) {
            velocity.y = Math.max( 0, velocity.y );
            canJump = true;
        } */

        controls.getObject().translateX( velocity.x * delta );
        // controls.getObject().translateY( velocity.y * delta );
        controls.getObject().translateZ( velocity.z * delta );

        // if (controls.getObject().position.y < 10) {
        //     velocity.y = 0;
        //     controls.getObject().position.y = 10;
        //     canJump = true;
        // }
        prevTime = time;
        TWEEN.update();
    }
    renderer.render(scene, camera);
    if ( renderer.xr.isPresenting ) {
        makeControllerTeleport(controller1);
    }
    /* raycaster.ray.origin.copy( controls.getObject().position );
    raycaster.ray.origin.y -= 10;*/
    if ( controls.isLocked === true ) {
        camera.getWorldPosition(cameraPosition);
        camera.getWorldDirection(cameraDirection);
        raycaster.set(cameraPosition, cameraDirection.normalize()); 

        intersections = raycaster.intersectObjects(raycastTargetScene.children, true);
		/* if( arrow !== undefined) {
        	scene.remove ( arrow );
		}
		arrow = new THREE.ArrowHelper( cameraDirection, cameraPosition, 100, Math.random() * 0xffffff );
		scene.add( arrow ); */

        onObject = intersections.length > 0;
        currObject = (onObject) ? intersections[0].object : undefined;
        currParent = (currObject && currObject.userData.parent) ? currObject.userData.parent : undefined;
        currRegion = (currParent && typeof currParent.getRegion === "function") ? currParent.getRegion() : undefined;
        if (!clickedDown) {
            if (gazeFSM.getState() == GazeStates.LookingAtObject) {
                if (!onObject) {
                    gazeFSM.dispatch(GazeEvents.LookingAtNothing);
                }
                if (onObject && prevObject != currObject) {
                    gazeFSM.dispatch(GazeEvents.LookingAtDifferentObject);
                }
            } else if (gazeFSM.getState() == GazeStates.Nothing) {
                if (onObject && !(currParent instanceof UseableFace)) {
                    gazeFSM.dispatch(GazeEvents.LookingAtObject);
                }
            }
        } else {
            if (gazeFSM.getState() == GazeStates.LookingAtObject) {
                gazeFSM.dispatch(GazeEvents.LookingAtNothing);
            }
        }
    }

    if (helmet && helmet.assetLoaded) {
        rotateObject(helmet.asset);
    }
    if (renderer.xr.isPresenting) {
        // In VR
        if (crosshair.visible) {
            crosshair.visible = false;
        }
        // Move everything down
    }
}




let onMouseDown = function() {
    clickedDown = true;
    if (onObject) {
        if ( !(lastClickedPoint.x == 0 && lastClickedPoint.y == 0 && lastClickedPoint.z == 0) &&
            intersections[0].point.distanceTo(lastClickedPoint) > clickDistance) {
                // It's a drag
                if(controlFSM.getState() == UserActionStates.Browse) {
                    controlFSM.dispatch(UserActionEvents.Drag);
                }
        }
        lastClickedPoint = intersections[0].point
    } else {
        lastClickedPoint.set(0, 0, 0);
    }
    if(isClick) {
        isClick = false;
    }
};

let onMouseUp = function() {
    clickedDown = false;
    if (onObject) {
        if (intersections[0].point.distanceTo(lastClickedPoint) < clickDistance) {
            // It's a click
            isClick = true;
            if (controlFSM.getState() == UserActionStates.Browse && onObject) {
                if (currObject.userData.parent.type == Icon.TYPE.Drag) {
                    // Pick up an object when clicking on the drag icon of the region
                    controlFSM.dispatch(UserActionEvents.DragClick);
                } else {
                    controlFSM.dispatch(UserActionEvents.OtherClick);
                }
            } 
        }
    }
    if (controlFSM.getState() == UserActionStates.RegionPickedUpClick || controlFSM.getState() == UserActionStates.RegionPickedUpDrag) {
        controlFSM.dispatch(UserActionEvents.DragEnd);
    }
    lastClickedPoint.set(0, 0, 0);
};

document.addEventListener('mousedown', onMouseDown, false);
document.addEventListener('mouseup', onMouseUp, false);

// State Changes
const controlOnClick = () => {
    staringAtFeed = true;
    if (onObject) {
        pickupObject(currRegion);
    }
};
const controlOnDrag = () => {
    staringAtFeed = true;
    if (!objectInHand && onObject) {
        pickupObject(currRegion);
    }
};
const controlDropObject = () => {
    // Object in Hand
    if (objectInHand) {
        pickedObject.localObject.scale.multiplyScalar(1 / 0.1);
        if(onObject && currRegion && currRegion != pickedObject) {
            // On another region
            RegionManager.mergeRegions(currRegion, pickedObject);
        } else {
            if (currObject.userData.parent) {
                // On a blank surface
                pickedObject.assignToFace(currObject.userData.parent);
                RegionManager.togglePin(pickedObject);
            }
            pickedObject.recreatePlane();
            pickedObject.resetAnimations();
        }
        objectInHand = false;
    }
    toggleWallVisibility();
    return controlFSM.dispatch(UserActionEvents.Reset);
};
const controlOtherAction = () => {
    if (onObject) {
        if (currObject.userData.parent instanceof Icon && currObject.userData.parent.type == Icon.TYPE.Pin) {
            RegionManager.togglePin(currRegion);
        } else if (currObject.userData.parent instanceof Icon
                   && (currObject.userData.parent.type == Icon.TYPE.Tick || currObject.userData.parent.type == Icon.TYPE.Untick)) {
                    // Menu Tick click
                    currObject.userData.parent.parentRegion.toggleTick();
        } else if (currParent && (currParent instanceof Image)) {
            currParent.open();
        }
    }
    return controlFSM.dispatch(UserActionEvents.Reset);
};
const controlTransitions = [
    /*      fromState                               event                           toState                                     callback */
    tFrom( UserActionStates.Browse,                 UserActionEvents.DragClick,     UserActionStates.RegionPickedUpClick,       controlOnClick ),
    tFrom( UserActionStates.Browse,                 UserActionEvents.Drag,          UserActionStates.RegionPickedUpDrag,        controlOnDrag ),
    tFrom( UserActionStates.Browse,                 UserActionEvents.OtherClick,    UserActionStates.OtherAction,               controlOtherAction ),
    tFrom( UserActionStates.OtherAction,            UserActionEvents.Reset,         UserActionStates.Browse,                    ()=>{}),
    tFrom( UserActionStates.RegionPickedUpDrag,     UserActionEvents.Drag,          UserActionStates.RegionPickedUpDrag,        controlOnDrag ),
    tFrom( UserActionStates.RegionPickedUpClick,    UserActionEvents.DragEnd,       UserActionStates.DropObject,                controlDropObject ),
    tFrom( UserActionStates.RegionPickedUpDrag,     UserActionEvents.DragEnd,       UserActionStates.DropObject,                controlDropObject ),
    tFrom( UserActionStates.DropObject,             UserActionEvents.Reset,         UserActionStates.Browse,                    ()=>{}),
];

const controlFSM = new StateMachine<UserActionStates, UserActionEvents>(UserActionStates.Browse, controlTransitions);

const lookAtObject = () => {
    staringAtFeed = true;
    if (prevObject) {
        // Taking the gaze off object, play reverse tween
        if(prevObject.userData.parent && prevObject.userData.parent.startReverseTween) {
            prevObject.userData.parent.stopTween();
            prevObject.userData.parent.startReverseTween();
        }
    }
    // If the new object has a tween, play it
    if(currObject.userData.parent && currObject.userData.parent.startTween) {
        currObject.userData.parent.startTween();
    }
    prevObject = currObject;
};
const lookAtNothing = () => {
    staringAtFeed = false;
    if (prevObject && prevObject.userData.parent && prevObject.userData.parent.startReverseTween) {
        if (prevObject.userData.parent.getRegion) {
            prevObject.userData.parent.getRegion().resetAnimations();
        }
        prevObject.userData.parent.stopTween();
        prevObject.userData.parent.startReverseTween();
        prevObject = undefined;
        staringAtFeed = false;
    }
};
const gazeTransitions = [
    /*      fromState                               event                           toState                                     callback */
    tFrom( GazeStates.Nothing,              GazeEvents.LookingAtObject,             GazeStates.LookingAtObject,     lookAtObject ),
    // tFrom( GazeStates.LookingAtObject,      GazeEvents.LookingAtObject,             GazeStates.LookingAtObject,     lookAtObject ),
    tFrom( GazeStates.LookingAtObject,      GazeEvents.LookingAtDifferentObject,    GazeStates.LookingAtObject,     lookAtObject ),
    tFrom( GazeStates.LookingAtObject,      GazeEvents.LookingAtNothing,            GazeStates.Nothing,             lookAtNothing ),
    tFrom( GazeStates.Nothing,              GazeEvents.LookingAtNothing,            GazeStates.Nothing,             () => {}),
];

const gazeFSM = new StateMachine<GazeStates, GazeEvents>(GazeStates.Nothing, gazeTransitions);

function pickupObject(region : Region) {
    let object3d = region.localObject;
    region.currentSurface.currentRegion = undefined;
    crosshair.attach(object3d);
    object3d.position.set(0.25, -0.25, -1);
    object3d.scale.multiplyScalar(0.1);
    objectInHand = true;
    pickedObject = region;
    toggleWallVisibility();
}

console.log("FSMs", gazeFSM, controlFSM);
