import * as THREE from "three";

import { UseableFace } from "./ARClasses/UseableFace";
import { ViewRectangleTracker } from "./ARClasses/ViewRectangleTracker";

import { SortedArrayMap } from 'collections/sorted-array-map';
import { Region } from "./ARClasses/Region";
import { RegionManager } from "./RegionManager";
import { MenuRegion } from "./ARClasses/MenuRegion";

export class WindowPlacementManager {

    private static _instance : WindowPlacementManager = new WindowPlacementManager();

    private static categoryMap : Map<string, SortedArrayMap<number, Region>>;
    private static faces : Map<string, UseableFace> = new Map();
    private static viewRectangleHeight = 150;
    private static viewRectangleWidth = WindowPlacementManager.viewRectangleHeight * Math.tan(60 * Math.PI / 180);
    //private static viewRectangleHeight = 100 * Math.tan(80);
    private static facesToConsider = 10;
    private static menuFaces : UseableFace[];

    constructor() {
        if(WindowPlacementManager._instance) {
            return;
        }
        WindowPlacementManager._instance = this;
        WindowPlacementManager.categoryMap = new Map();
        WindowPlacementManager.menuFaces = [];
        console.log("WindowPlacementManager", WindowPlacementManager);
    }

    public static appendObjects(objects : UseableFace[]) {
        for(let element of objects) {
            this.faces.set(element.id, element);
        }
    }

    public static addMarker(marker : Region) {
        let priorityMap : SortedArrayMap<number, any>;
        //console.log("map", this.markerPriorityMap);
        if(this.categoryMap.has(marker.category)) {
            priorityMap = this.categoryMap.get(marker.category);
            //console.log("got map", marker.tracker_type)
        } else {
            //console.log("dint got map", marker.tracker_type)
            priorityMap = new SortedArrayMap(undefined, undefined, (left, right) => { return left < right; });
        }
        priorityMap.set(marker.priority, marker);
        this.categoryMap.set(marker.category, priorityMap);
        //console.log("new map", this.categoryMap);
    }

    public static addMarkers(markers : Region[]) {
        markers.forEach(element => this.addMarker(element));
    }

    public static deleteMarker(marker : Region) {
        if(this.categoryMap.has(marker.category)) {
            if(this.categoryMap.get(marker.category).has(marker.priority)) {
                this.categoryMap.get(marker.category).delete(marker.priority);
            }
        }
    }

    public static addFaces(faces : UseableFace[]) {
        for(let face of faces) {
            this.faces.set(face.id, face);
        }
    }

    public static deleteFaces(faces : UseableFace[]) {
        for(let face of faces) {
            this.faces.delete(face.id);
        }
    }

    public static getInstance() : WindowPlacementManager {
        return WindowPlacementManager._instance;
    }

    public static setMarkerPositions(camera : THREE.Camera) {
        let cameraWorldPosition : THREE.Vector3 = new THREE.Vector3(0, 0, 0);
        camera.getWorldPosition(cameraWorldPosition);

        let closestFaces : Map<string, UseableFace[]> = findClosestFaces(cameraWorldPosition, [...this.faces.values()]);

        if(closestFaces != undefined) {
            for(let [category, markerMap] of this.categoryMap) {
                let indexMoved = 0;
                for(let [priority, marker] of markerMap.enumerate()) {
                    if(closestFaces.has(category)) {
                        let face = closestFaces.get(category)[indexMoved];
                        marker.assignToFace(face);
                        face.currentRegion = marker;
                        indexMoved++;
                    }
                }
            }
        }
    }

    public static addMenuFaces(faces : UseableFace[]) {
        faces.map((val) => {
            this.menuFaces.push(val);
        });
    }

    public static resetMenuFaces() {
        this.menuFaces = [];
    }

    public static alignWithMenu() {
        let regions = RegionManager.getAllMenuRegions();
        if (regions.length < this.menuFaces.length) {
            // Show the menu if enough space
            regions.map((value, index) => {
                value.assignToFace(this.menuFaces[index]);
            });
        }
    }
}


// Circle distance functions
function findClosestFaces(basePosition : THREE.Vector3, facesArray : UseableFace[]) : Map<string, UseableFace[]> {
    let catMap : Map<string, SortedArrayMap<number, UseableFace>> = new Map();
    let sortedMap : SortedArrayMap<number, UseableFace>;
    for (let i = 0; i < facesArray.length; i++) {
        let distance : number = dist(facesArray[i].faceCentroid, basePosition);
        if(catMap.has(facesArray[i].category)) {
            sortedMap = catMap.get(facesArray[i].category);
        } else {
            sortedMap = new SortedArrayMap();
        }
        sortedMap.set(distance, facesArray[i]);
        catMap.set(facesArray[i].category, sortedMap);
        //console.log(distance, facesArray[i].category);
    }
    let categoryArray : Map<string, UseableFace[]> = new Map(); // This is a map of the category and Faces sorted by distance
    for(let [category, distMap] of catMap) {
        categoryArray.set(category, [...distMap.values()]);
    }
    return categoryArray;
}


function dist(inp1 : THREE.Vector3, inp2 : THREE.Vector3) : number {
    return Math.pow(inp1.x - inp2.x, 2) + Math.pow(inp1.z - inp2.z, 2);// + Math.pow(inp1.y - inp2.y, 2);
}

