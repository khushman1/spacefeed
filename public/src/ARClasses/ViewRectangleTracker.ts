import * as THREE from "three";

const uuidv4 = require('uuid/v4');

import { Anchor } from "./Anchor";
import { ARAnchor } from "./ARAnchor";
import { UseableFace } from "./UseableFace";

export class ViewRectangleTracker implements ARAnchor, Anchor {
    enabled: boolean;
    id : string;
    assets: any[];
    tracker_type : ViewRectangleTracker.TYPE;
    priority : number;
    faces : UseableFace[];
    category : string;
    orient : boolean;
    tempVector : THREE.Vector3;

    constructor(tracker_type : ViewRectangleTracker.TYPE, priority : number, category : string, assets? : any[]) {
        this.tracker_type = tracker_type;
        this.priority = priority;
        this.category = category;
        if(assets !== undefined) {
            this.assets = assets;
        }
        this.id = '' + Math.round(Date.now());
        this.orient = true;
        this.tempVector = new THREE.Vector3();
    }

    addToScene(scene : THREE.Scene) {
        for(let i = 0; i < this.assets.length; i++) {
            this.assets[i].addToScene(scene);
        }
    }

    assignToFaces(faces : UseableFace[]) {
        let i : number;
        for(i = 0; i < this.assets.length; i++) {
            if(i < faces.length && this.assets[i].assetLoaded) {
                this.alignObjectToUseableFace(faces[i], this.assets[i].asset);
            }
        }
            /*if(this.assets.length > 0) {
            this.alignAssetsToUseableFace(faces[0]);
        }*/
        return i;
    }
        /*
    assignToFaces(objsLeft, objsRight) {
        let i : number = 0;
        let faces : UseableFace[];
        if(this.tracker_type == ViewRectangleTracker.TYPE.ViewRectangleTrackerLeft) {
            faces = objsLeft;
        } else if(this.tracker_type == ViewRectangleTracker.TYPE.ViewRectangleTrackerRight) {
            faces = objsRight;
        }
        for(i = 0; i < this.assets.length; i++) {
            if(i < faces.length) {
                alignObjectToUseableFace(faces[i], this.assets[i].asset, true);
            }
        }
        return [i, this.tracker_type];
    }*/

    alignObjectToUseableFace(useableFace : UseableFace, objectToAlign : THREE.Object3D) {
        let worldPosition : THREE.Vector3 = useableFace.faceCentroid.clone();
        let offsetPosition : THREE.Vector3 = getOffsetAlongNormal(worldPosition, useableFace.normal, 0.1);
        objectToAlign.position.set(offsetPosition.x, offsetPosition.y, offsetPosition.z);
        if (this.orient) {
            orientAlongNormal(objectToAlign, useableFace.normal);
        }
    }

    alignAssetsToUseableFace(useableFace : UseableFace) {
        let noAssets = this.assets.length;
        let placementPoints : THREE.Vector3[] = [];
        let center : THREE.Vector3 = useableFace.faceCentroid;
        let surface = useableFace.surface;
        if(noAssets <= 1) {
            placementPoints.push(center);
        } else {
            //let offsetPosition : THREE.Vector3 = getOffsetAlongNormal(worldPosition, useableFace.normal, 0.1);
            let width = (useableFace.surface.geometry as THREE.PlaneBufferGeometry).parameters.width;
            let height = (useableFace.surface.geometry as THREE.PlaneBufferGeometry).parameters.height;
            let placementCircleRadius = ((width > height) ? height : width) / 2;
            let centralAngle = 360 / noAssets;
            for(let i = 0; i < noAssets; i++) {
                this.tempVector.x = Math.sin(centralAngle * i) * placementCircleRadius;
                this.tempVector.y = Math.cos(centralAngle * i) * placementCircleRadius;
                this.tempVector.z = 0;
                placementPoints.push(this.tempVector.clone());
            }
            for(let i = 0; i < noAssets; i++) {
                this.tempVector = getOffsetAlongNormal(placementPoints[i], useableFace.normal, 0.1);
                this.assets[i].asset.position.set(this.tempVector.x, this.tempVector.y, this.tempVector.z);
                if (this.orient) {
                    orientAlongNormal(this.assets[i].asset, useableFace.normal);
                }
            }
        console.log("assets along", surface, center, useableFace.normal, placementPoints);
        }
    }
}


export namespace ViewRectangleTracker {
    export enum TYPE {
        ViewRectangleTrackerLeft = 1,
            ViewRectangleTrackerRight,
    }
}


function getOffsetAlongNormal(position : THREE.Vector3, normal : THREE.Vector3, scale : number) : THREE.Vector3 {
    return new THREE.Vector3(
        position.x + normal.x * scale,
        position.y + normal.y * scale,
        position.z + normal.z * scale);
}


// Orient the Object along the normal specified
function orientAlongNormal(objectToOrient : THREE.Object3D, normal : THREE.Vector3) : void {
    let coords : THREE.Vector3 = objectToOrient.position;
    let newPoint : THREE.Vector3 = new THREE.Vector3(
        coords.x + normal.x,
        coords.y + normal.y,
        coords.z + normal.z
    );
    objectToOrient.lookAt( newPoint );
}
