import * as THREE from "three";

import { Feature } from "./Feature";

export class ARMLContext {
    id : string;
    features : Feature[];

    constructor(id : string, features? : Feature[]) {
        this.id = id;
        if(features !== undefined) {
            this.features = features;
        } else {
            this.features = [];
        }
    }

    addFeatures(features : Feature[]) {
        this.features = this.features.concat(features);
    }

    addContextToScene(scene : THREE.Scene) {
        for(let i = 0; i < this.features.length; i++) {
            this.features[i].addToScene(scene);
        }
    }
}
