import * as THREE from "three";
const TWEEN = require('es6-tween');

import { ARElement } from "./ARElement";
import { VisualAsset } from "./VisualAsset";
import { Region } from "./Region";
import * as _u from "../utils/util"


let timeout = 100;
let iconSize = 0.5;

export class Image implements VisualAsset {
    id : string;
    enabled : boolean;
    filePath : string;
    link : string;
    asset : THREE.Mesh;
    assetLoaded : boolean;
    fadePath : string;
    fadeAsset : THREE.Mesh;
    fadeAssetLoaded : boolean;
    tween : any;
    reverseTween : any;
    parentRegion : Region;
    size : number;

    constructor(scene : THREE.Scene, filePath : string, size : number, fadeFilePath? : string, link? : string) {
        if (!link) link = "";
        let { imageGeometry, imageMaterial } = this.imageInit(filePath, (fadeFilePath) ? iconSize : size);
        this.asset = _u.createImage(imageGeometry, imageMaterial);
        this.size = size;
		this.asset.userData = {"parent": this};
        if (fadeFilePath !== undefined) {
            this.fadePath = fadeFilePath;
            let { imageGeometry, imageMaterial } = this.imageInit(fadeFilePath, size, true);
            imageMaterial.opacity = 0.0;
            imageMaterial.visible = false;
            this.fadeAsset = _u.createImage(imageGeometry, imageMaterial);
			this.fadeAsset.userData = {"parent": this};
            this.createTween();
        }
        this.assetLoaded = true;
        this.link = link;
        this.filePath = filePath;
        this.fadeAssetLoaded = false;
        this.addToScene(scene);
    }

    getRegion() {
        return this.parentRegion;
    }

    setRegion(region : Region) {
        this.parentRegion = region;
    }

    addToScene(scene : THREE.Object3D) {
        this.addAsset(scene);
    }

    startTween() {
        this.tween.start();
        this.tween.play();
        if(this.parentRegion) {
            this.parentRegion.startTween();
        }
    }

    startReverseTween() {
        this.reverseTween.start();
        this.reverseTween.play();
        if(this.parentRegion) {
            this.parentRegion.startReverseTween();
        }
    }

    stopTween() {
        this.tween.stop();
        if(this.parentRegion) {
            this.parentRegion.stopTween();
        }
    }

    stopReverseTween() {
        this.reverseTween.stop();
        if(this.parentRegion) {
            this.parentRegion.stopReverseTween();
        }
    }

    createTween() {
        let opacity = 0.0;
        this.tween = new TWEEN.Tween({opacity: 0.0}).to({opacity: 1.0}, 500)
            .on('start', () => {(this.fadeAsset.material as THREE.Material).visible = true;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let fadeMaterial : THREE.Material = this.fadeAsset.material as THREE.Material;
                let material : THREE.Material = this.asset.material as THREE.Material;
                material.opacity = 1.0 - opacity;
                fadeMaterial.opacity = opacity;
            })
            // .on('complete', () => {(this.asset.material as THREE.Material).visible = false;});
        this.reverseTween = new TWEEN.Tween({opacity: 1.0}).to({opacity: 0.0}, 500)
            .on('start', () => {(this.asset.material as THREE.Material).visible = true;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let fadeMaterial : THREE.Material = this.fadeAsset.material as THREE.Material;
                let material : THREE.Material = this.asset.material as THREE.Material;
                material.opacity = 1.0 - opacity;
                fadeMaterial.opacity = opacity;
            })
            // .on('complete', () => {(this.fadeAsset.material as THREE.Material).visible = false;});
    }

    imageInit(filePath : string, size : number, changeFlag? : boolean) {
        let imageLoader : THREE.TextureLoader = new THREE.TextureLoader();
        let aspectRatio : number = 1;
        let currScale = 1;
        let imageGeometry : THREE.PlaneBufferGeometry = new THREE.PlaneBufferGeometry(size, size);
        let imageMaterial : any = new THREE.MeshBasicMaterial({
            map: imageLoader.load( filePath, (texture) => {
                if(changeFlag) {
                    this.fadeAsset.scale.set(1 / currScale, (texture.image.height / texture.image.width) / currScale, 1 / currScale);
                    this.asset.add(this.fadeAsset);
                    this.fadeAssetLoaded = true;
                    this.fadeAsset.position.z += 0.1;
                } else {
                    currScale = 0.5;
                    if(this.fadePath == undefined) {
                        currScale = 1;
                    }
                    this.asset.scale.set(currScale, currScale * texture.image.height / texture.image.width, currScale);
                }
            }),
            transparent: true,
            side: THREE.DoubleSide
        });
        return { imageGeometry, imageMaterial };
    }

    addAsset(scene) {
        if(this.assetLoaded) {
            scene.add(this.asset);
        } else {
            setTimeout(() => {this.addAsset(scene)}, timeout);
        }
    }

    addFadeAsset(scene) {
        if(this.fadeAssetLoaded) {
            scene.add(this.fadeAsset);
        } else {
            setTimeout(() => {this.addFadeAsset(scene)}, timeout);
        }
    }

    getMaxSize() {
        let size : THREE.Vector2 = new THREE.Vector2(0, 0);
        let assetParams = (this.asset.geometry as THREE.PlaneBufferGeometry).parameters;
        let fadeAssetParams = (this.fadeAsset && (this.fadeAsset.geometry as THREE.PlaneBufferGeometry).parameters);
        size.x = Math.max(assetParams.width * this.asset.scale.x, this.fadeAsset && fadeAssetParams.width * this.fadeAsset.scale.x * this.asset.scale.x || 0);
        size.y = Math.max(assetParams.height * this.asset.scale.y, this.fadeAsset && fadeAssetParams.height * this.fadeAsset.scale.y * this.asset.scale.y || 0);
        return size;
    }

    copy(scene : THREE.Scene) {
        return new Image(scene, this.filePath, this.size, this.fadePath);
    }

    open() {
        if (window && this.link != "") {
            window.open(this.link, "_blank");
        }
    }
}

