import * as THREE from "three";
const TWEEN = require('es6-tween');

import { Anchor } from "./Anchor";
import { ARAnchor } from "./ARAnchor";
import { UseableFace } from "./UseableFace";
import { Icon } from "./Icon";
import * as _u from "../utils/util"

let padding = 0.1;
let maxOpacity = 0.2;

export class Region implements ARAnchor, Anchor {
    id : string;
    enabled : boolean;
    assets: any[];
    priority : number;
    orient : boolean;
    category : string;
    localObject : THREE.Mesh;
    wireframe : THREE.LineSegments;
    tempVector : THREE.Vector3;
    size : THREE.Vector2;
    tween : any;
    reverseTween : any;
    currentSurface : UseableFace;
    pinIcon : Icon;
    pinned : boolean;
    dragIcon : Icon;
    static scene : THREE.Scene;
    public static baseColor: THREE.Color = new THREE.Color("#0000ff");
    public static pinnedColor : THREE.Color = new THREE.Color("#e60073");

    constructor(scene : THREE.Scene, priority : number, category : string, assets? : any[]) {
        Region.scene = scene;
        this.priority = priority;
        this.category = category;
        if(assets !== undefined) {
            this.assets = assets;
        }
        this.id = '' + Math.round(Date.now()) + 10 * Math.random();
        this.orient = true;
        this.pinned = false;
        this.tempVector = new THREE.Vector3(0, 0, 1);
        this.createTween();
        this.createIcons();
    }

    dispose() {
        if(this.pinIcon) {
            this.pinIcon.dispose();
        }
        if(this.dragIcon) {
            this.dragIcon.dispose();
        }
        this.localObject.geometry.dispose();
        (this.localObject.material as THREE.Material).dispose();
        this.pinIcon = undefined;
        this.dragIcon = undefined;
        if(this.localObject) {
            delete this.localObject;
        }
    }

    startTween() {
        this.tween.start();
        this.tween.play();
        if(this.pinIcon) {
            this.pinIcon.visibleTween.start();
            this.pinIcon.visibleTween.play();
        }
        if(this.dragIcon) {
            this.dragIcon.visibleTween.start();
            this.dragIcon.visibleTween.play();
        }
    }

    startReverseTween() {
        this.reverseTween.start();
        this.reverseTween.play();
        if(this.pinIcon) {
            this.pinIcon.reverseVisibleTween.start();
            this.pinIcon.reverseVisibleTween.play();
        }
        if(this.dragIcon) {
            this.dragIcon.reverseVisibleTween.start();
            this.dragIcon.reverseVisibleTween.play();
        }
    }

    resetAnimations() {
        (this.localObject.material as THREE.Material).opacity = 0.0;
        this.stopTween();
        this.stopReverseTween();
        if(this.pinIcon) {
            this.pinIcon.resetAnimations();
        }
        if(this.dragIcon) {
            this.dragIcon.resetAnimations();
        }
    }


    stopTween() {
        if(this.tween.isPlaying()) {
            this.tween.stop();
        }
        if(this.pinIcon && this.pinIcon.visibleTween.isPlaying()) {
            this.pinIcon.visibleTween.stop();
        }
        if(this.dragIcon && this.dragIcon.visibleTween.isPlaying()) {
            this.dragIcon.visibleTween.stop();
        }
    }

    stopReverseTween() {
        if(this.reverseTween.isPlaying()) {
            this.reverseTween.stop();
        }
        if(this.pinIcon && this.pinIcon.reverseVisibleTween.isPlaying()) {
            this.pinIcon.reverseVisibleTween.stop();
        }
        if(this.dragIcon && this.dragIcon.reverseVisibleTween.isPlaying()) {
            this.dragIcon.reverseVisibleTween.stop();
        }
    }

    createTween() {
        this.tween = new TWEEN.Tween({opacity: 0.0}).to({opacity: maxOpacity}, 200)
            .on('start', () => {(this.localObject.material as THREE.Material).opacity = 0.0;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let material : THREE.Material = this.localObject.material as THREE.Material;
                material.opacity = opacity;
            });
        this.reverseTween = new TWEEN.Tween({opacity: maxOpacity}).to({opacity: 0.0}, 200)
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let material : THREE.Material = this.localObject.material as THREE.Material;
                material.opacity = opacity;
            });
    }

    createPlane() {
        this.createIcons();
        this.size = new THREE.Vector2(0, 0);
        for(let i = 0; i < this.assets.length; i++) {
            let currSize : THREE.Vector2 = this.assets[i].getMaxSize();
            this.size.x += currSize.x;
            this.size.y = Math.max(this.size.y, currSize.y);
        }
        this.size.x += padding;
        this.size.y += padding * 2 + this.pinIcon.size;
        let geometry : THREE.PlaneBufferGeometry = new THREE.PlaneBufferGeometry(this.size.x, this.size.y);
        let material : THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: Region.baseColor, opacity: 0.0, transparent: true});
        let plane : THREE.Mesh = new THREE.Mesh(geometry, material);
        this.localObject = plane;
        this.localObject.userData = {"parent": this};
        this.assignColor();

        // wireframe
        let geo = new THREE.EdgesGeometry(this.localObject.geometry); // or WireframeGeometry
        let mat = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 1 });
        this.wireframe = new THREE.LineSegments(geo, mat);
        this.localObject.add(this.wireframe);

        this.pinIcon.addToObject(this.localObject);
        this.dragIcon.addToObject(this.localObject);
        this.tempVector.set(-( this.pinIcon.size + padding ) / 2, (this.size.y - this.pinIcon.size - padding) / 2, 0.01);
        this.pinIcon.setPosition(this.tempVector);
        this.tempVector.set(( this.dragIcon.size + padding ) / 2, (this.size.y - this.dragIcon.size - padding) / 2, 0.01);
        this.dragIcon.setPosition(this.tempVector);
    }

    recreatePlane() {
        if(this.localObject != undefined) {
            for(let child of this.localObject.children) {
                this.localObject.remove(child);
            }
            delete this.localObject;
        }
        this.localObject = undefined;
        this.createRegion();
        if(this.currentSurface) {
            this.assignToFace(this.currentSurface);
        }
    }

    createRegion() {
        this.addAssetsToRegion();
        this.addToScene(Region.scene);
    }

    addAssetsToRegion() {
        if(this.localObject == undefined) {
            this.createPlane();
        }
        let x : number = - this.size.x / 2;
        x += padding / 2;
        for(let i = 0; i < this.assets.length; i++) {
            let assetSize = this.assets[i].getMaxSize();
            x += assetSize.x / 2;
            this.localObject.add(this.assets[i].asset);
            this.assets[i].setRegion(this);
            this.assets[i].asset.position.x = x;
            this.assets[i].asset.position.y = -this.pinIcon.size;
            this.tempVector.set(0, 0, 1);
            this.alignObjectToUseableFace(this.assets[i].asset.position, this.tempVector, this.assets[i].asset);
            x += assetSize.x / 2;
        }
        x += padding / 2;
        //this.localObject.rotation.y = Math.PI / 2;
    }

    appendAssets(assets : any[]) {
        this.assets.push(...assets);
    }

    addToScene(scene? : THREE.Scene) {
        if (!scene) scene = Region.scene;
        scene.add(this.localObject);
    }

    assignToFace(face : UseableFace) {
        if(this.localObject.position != face.faceCentroid) {
            this.alignObjectToUseableFace(face.faceCentroid, face.normal, this.localObject);
            this.currentSurface = face;
        }
    }

    createIcons() {
        if (this.pinIcon == undefined) {
            this.pinIcon = new Icon(this, Icon.TYPE.Pin);
        }
        if (this.dragIcon == undefined) {
            this.dragIcon = new Icon(this, Icon.TYPE.Drag);
        }
    }

    alignObjectToUseableFace(position : THREE.Vector3, normal : THREE.Vector3, objectToAlign : THREE.Object3D) {
        let worldPosition : THREE.Vector3 = position;//useableFace.faceCentroid.clone();
        objectToAlign.position.copy(getOffsetAlongNormal(worldPosition, normal, 0.05));
        if (this.category == "menu") {
            objectToAlign.lookAt(normal.multiplyScalar(100000));
        } else if (this.orient) {
            orientAlongNormal(objectToAlign, normal);
        }
    }

    togglePin() {
        this.pinned = !this.pinned;
        this.assignColor();
    }

    private assignColor() {
        // Change pin colour here
        let selectedColor : THREE.Color;
        if(this.pinned) {
            selectedColor = Region.pinnedColor;
        } else {
            selectedColor = Region.baseColor;
        }
        (this.localObject.material as THREE.MeshBasicMaterial).color = selectedColor;
    }

    getRegion() {
        return this;
    }

        /*alignAssetsToUseableFace(useableFace : UseableFace) {
        let noAssets = this.assets.length;
        let placementPoints : THREE.Vector3[] = [];
        let center : THREE.Vector3 = useableFace.faceCentroid;
        let surface = useableFace.surface;
        if(noAssets <= 1) {
            placementPoints.push(center);
        } else {
            //let offsetPosition : THREE.Vector3 = getOffsetAlongNormal(worldPosition, useableFace.normal, 0.1);
            let width = (useableFace.surface.geometry as THREE.PlaneBufferGeometry).parameters.width;
            let height = (useableFace.surface.geometry as THREE.PlaneBufferGeometry).parameters.height;
            let placementCircleRadius = ((width > height) ? height : width) / 2;
            let centralAngle = 360 / noAssets;
            for(let i = 0; i < noAssets; i++) {
                this.tempVector.x = Math.sin(centralAngle * i) * placementCircleRadius;
                this.tempVector.y = Math.cos(centralAngle * i) * placementCircleRadius;
                this.tempVector.z = 0;
                placementPoints.push(this.tempVector.clone());
            }
            for(let i = 0; i < noAssets; i++) {
                this.tempVector = getOffsetAlongNormal(placementPoints[i], useableFace.normal, 0.1);
                this.assets[i].asset.position.set(this.tempVector.x, this.tempVector.y, this.tempVector.z);
                if (this.orient) {
                    orientAlongNormal(this.assets[i].asset, useableFace.normal);
                }
            }
        console.log("assets along", surface, center, useableFace.normal, placementPoints);
        }
    }*/
}


export namespace Region {
    export enum TYPE {
        ViewRectangleTrackerLeft = 1,
            ViewRectangleTrackerRight,
    }
}


function getOffsetAlongNormal(position : THREE.Vector3, normal : THREE.Vector3, scale : number) : THREE.Vector3 {
    let rando : number = Math.random();
    return new THREE.Vector3(
        position.x + (rando + normal.x) * scale,
        position.y + (rando + normal.y) * scale,
        position.z + (rando + normal.z) * scale);
}


// Orient the Object along the normal specified
function orientAlongNormal(objectToOrient : THREE.Object3D, normal : THREE.Vector3) : void {
    let coords : THREE.Vector3 = objectToOrient.position;
    let newPoint : THREE.Vector3 = new THREE.Vector3(
        coords.x + normal.x,
        coords.y + normal.y,
        coords.z + normal.z
    );
    objectToOrient.lookAt( newPoint );
}
