import * as THREE from "three";

import { ARElement } from "./ARElement";

export class Feature implements ARElement {
    id : string;
    name : string;
    description : string;
    enabled : boolean;
    metadata : string;
    anchors : any[];

    constructor(name : string, description : string, enabled? : boolean, metadata? : string, anchors? : any[]) {
        this.name = name;
        this.anchors = [];
        this.description = description;
        if(enabled !== undefined) {
            this.enabled = enabled;
        }
        if(metadata !== undefined) {
            this.metadata = metadata;
        }
        if(anchors !== undefined) {
            this.anchors = anchors;
        }
    }

    addAnchors(anchors : any[]) {
        this.anchors = this.anchors.concat(anchors);
    }

    addToScene(scene : THREE.Scene) {
        console.log("anch", this.anchors);
        for(let i = 0; i < this.anchors.length; i++) {
            this.anchors[i].addToScene(scene);
        }
    }
}
