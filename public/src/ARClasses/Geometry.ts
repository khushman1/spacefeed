import * as THREE from "three";

import { Anchor } from "./Anchor";
import { ARAnchor } from "./ARAnchor";

export class Geometry implements ARAnchor, Anchor {
    enabled : boolean;
    geometry_type: Geometry.TYPE;
    point : THREE.Vector3;
    region : THREE.Vector3[];
    assets : any[];

    constructor(geom_type : Geometry.TYPE, pointArray : THREE.Vector3[], assets? : any[], enabled? : boolean) {
        if(!assets) assets = [];
        if(!enabled) enabled = false;
        this.geometry_type = geom_type;
        if(this.geometry_type == Geometry.TYPE.Point) {
            this.point = pointArray[0];
        } else {
            this.region = pointArray;
        }
        this.assets = assets;
        this.enabled = enabled;
        if (enabled) {
            assets.map((asset, i) => {this.checkPositionTimeouts(i);});
        }
    }

    addToScene(scene : THREE.Scene) {
        console.log("assets", this.assets);
        for(let i = 0; i < this.assets.length; i++) {
            // checkPositionTimeouts(this, i);
            this.assets[i].addToScene(scene);
        }
    }

    positionAsset(asset : THREE.Object3D) {
        switch(this.geometry_type) {
            case Geometry.TYPE.Point : {
                //console.log("geom", this);
                asset.position.copy(this.point);
                break;
            }
        }
    }

    checkPositionTimeouts(i : number) {
        if(this.assets[i].assetLoaded) {
            this.positionAsset(this.assets[i].asset);
        } else {
            setTimeout(() => {this.checkPositionTimeouts(i);}, 200);
        }
    }
}

export namespace Geometry {
    export enum TYPE {
        Point = 1,
            Region,
    }
}


