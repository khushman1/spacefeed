import * as THREE from "three";
import { Region } from "./Region";

export class UseableFace {
    faceCentroid : THREE.Vector3;
    normal : THREE.Vector3;
    surface : THREE.Mesh;
    category: string;
    id : string;
    currentRegion : Region | undefined;

    constructor(faceCentroid : THREE.Vector3, normal : THREE.Vector3, surface : THREE.Mesh, face_category?: string) {
        this.faceCentroid = faceCentroid;
        this.normal = normal;
        this.surface = surface;
        this.id = '' + Math.round(Date.now()) + 10 * Math.random();
        if(face_category) {
            this.category = face_category;
        }
        this.currentRegion = undefined;
    }

    getRegion() {
        return this.currentRegion;
    }
}
