import * as THREE from "three";

import { VisualAsset } from "./VisualAsset";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

export class Model implements VisualAsset {
    id : string;
    enabled : boolean;
    fileName : string;
    filePath : string;
    asset : any;
    assetLoaded : boolean;
    background : any;
    scale: THREE.Vector3;


    constructor(fileName : string, {filePath = undefined, background = undefined, enabled = true, scale = undefined} = {}) {
        this.fileName = fileName;
        this.assetLoaded = false;
        if(filePath !== undefined) {
            this.filePath = filePath;
        }
        this.enabled = enabled;
        if(background !== undefined) {
            this.background = background;
        }
        if(scale !== undefined) {
            this.scale = scale;
        }
        this.createAsset();
    }

    createAsset() {
        this.loadGLTFModel(this.filePath, this.fileName, this.background);
    }

    loadGLTFModel(filePath: string, fileName: string, background?: any) {
        let gltfRef : THREE.Scene;
        let gloader = new GLTFLoader();
        gloader.setPath(filePath);
        gloader.load(fileName, (gltf) => {
            gltf.scene.traverse((child : THREE.Mesh) => {
                if (child.isMesh) {
                    (child.material as THREE.MeshBasicMaterial).envMap = background;
                    child.userData = {"parent": this};
                }
            });

            this.asset = gltf.scene;
            this.asset.userData = {"parent": this};
            this.assetLoaded = true;
            
            if(this.scale) {
                gltf.scene.scale.set(this.scale.x, this.scale.y, this.scale.z);
            }
        }, undefined, function (e) {
            console.error(e);
        } );
    }

    addToScene(scene : THREE.Scene) {
        checkAddTimeouts(this, scene);
    }
}


function checkAddTimeouts(assetInstance : Model, scene: THREE.Scene) {
    if(assetInstance.assetLoaded) {
        scene.add(assetInstance.asset);
    } else {
        setTimeout(function() {
            checkAddTimeouts(assetInstance, scene);
        }, 500);
    }
}
