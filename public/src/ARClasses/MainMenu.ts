import * as THREE from "three";
const TWEEN = require('es6-tween');

import * as _u from "../utils/util";
import { MenuRegion } from "./MenuRegion";
import { UseableFace } from "./UseableFace";
import { WindowPlacementManager } from "../WindowPlacementManager";
import { RegionManager } from "../RegionManager";

export class MainMenu {
    id : string;
    imagePath = "./images/menu.png"
    menuButtonSize = 2 / 7;
    menuButtonScale = 0.1 / 7;
    menuButtonPosition = new THREE.Vector3(-0, -0.5 / 7, -1 / 7);
    button : THREE.Mesh;
    localObject : THREE.Mesh;
    localObjectEdges : THREE.LineSegments;
    localObjectPosition : THREE.Vector3 = new THREE.Vector3(0, 0, -3);
    cylinderRadialLength : number = Math.PI - (Math.PI / 3.5);
    tween : any;
    reverseTween : any;

    constructor() {
        this.id = "mainmenu";
        this.createButton();
        this.createCylinder();
        this.createTween();
        MenuRegion.parentCylinder = this.localObject;
        this.hideMenu();
    }

    createButton() {
        let { imageGeometry, imageMaterial } = _u.imageInit(this.imagePath, this.menuButtonSize);
        imageMaterial.opacity = 0.4;
        this.button = _u.createImage(imageGeometry, imageMaterial);
        this.button.scale.set(this.menuButtonScale, this.menuButtonScale, this.menuButtonScale);
        this.button.position.copy(this.menuButtonPosition);
    }

    createCylinder() {
        let cylinder = new THREE.CylinderBufferGeometry(6 / 7, 6 / 7, 9 / 7, 5, 3, true, - this.cylinderRadialLength / 2, this.cylinderRadialLength);
        let material = new THREE.MeshBasicMaterial({color: "#222", opacity: 0.0, side: THREE.DoubleSide, transparent: true});
        this.localObject = new THREE.Mesh(cylinder, material);
        this.localObject.position.copy(this.localObjectPosition);
        console.log("menuCylinder", this.localObject);
        let positionedFaces = parseUseableFacesFromCylinder(this.localObject);

        // wireframe
        var geo = new THREE.EdgesGeometry(this.localObject.geometry); // or WireframeGeometry
        var mat = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
        this.localObjectEdges = new THREE.LineSegments(geo, mat);
        this.localObject.add(this.localObjectEdges);

        positionedFaces.map((row) => WindowPlacementManager.addMenuFaces(row));
    }

    addToScene(scene : THREE.Scene, parentObject : THREE.Object3D) {
        // parentObject.add(this.button);
        scene.add(this.localObject);
    }

    showMenu(cameraPosition : THREE.Vector3, cameraDirection : THREE.Vector3) {
        this.startTween();
        // (this.button.material as THREE.MeshBasicMaterial).color.set("#bb2222");
        this.button.visible = false;
        let target = cameraPosition.clone();
        target.add(cameraDirection.clone().normalize().multiplyScalar(2 / 7));
        this.localObject.position.copy(target);
        this.localObject.lookAt(cameraPosition);
        this.localObject.rotateOnAxis(new THREE.Vector3(0, 1, 0), - Math.PI);
    }

    hideMenu() {
        this.startReverseTween();
        this.button.visible = true;
        this.localObject.position.set(0, 1000, 0);
    }

    toggleMenu(cameraPosition : THREE.Vector3, cameraDirection : THREE.Vector3) {
        if (this.button.visible) {
            this.showMenu(cameraPosition, cameraDirection);
        } else {
            this.hideMenu();
        }
    }

    startTween() {
        this.tween.start();
        this.tween.play();
    }

    startReverseTween() {
        this.reverseTween.start();
        this.reverseTween.play();
    }

    stopTween() {
        if(this.tween.isPlaying()) {
            this.tween.stop();
        }
    }

    stopReverseTween() {
        if(this.reverseTween.isPlaying()) {
            this.reverseTween.stop();
        }
    }

    createTween() {
        let maxOpacity = 0.4;
        this.tween = new TWEEN.Tween({opacity: 0.0}).to({opacity: maxOpacity}, 200)
            .on('start', () => {(this.localObject.material as THREE.Material).opacity = 0.0;
                                (this.button.material as THREE.Material).opacity = maxOpacity;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let material : THREE.Material = this.localObject.material as THREE.Material;
                material.opacity = opacity;
                (this.button.material as THREE.Material).opacity = maxOpacity - opacity;
                (this.localObjectEdges.material as THREE.Material).opacity = opacity;
            });
        this.reverseTween = new TWEEN.Tween({opacity: maxOpacity}).to({opacity: 0.0}, 200)
            .on('start', () => {(this.localObject.material as THREE.Material).opacity = maxOpacity;
                                (this.button.material as THREE.Material).opacity = 0.0;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let material : THREE.Material = this.localObject.material as THREE.Material;
                material.opacity = opacity;
                (this.button.material as THREE.Material).opacity = maxOpacity - opacity;
                (this.localObjectEdges.material as THREE.Material).opacity = opacity;
            });
    }
}

function parseUseableFacesFromCylinder(mesh : THREE.Mesh) {
    // right to left, top to bottom, new row fresh are points and normal is 0-6 x 0-1
    // And you have to consider points 0 and 7 for midpoint, 0, 4 in each 6(2 triangles)
    let cylinder : THREE.CylinderBufferGeometry = mesh.geometry as THREE.CylinderBufferGeometry;
    let parameters = cylinder.parameters;
    let triangles = cylinder.index.array;
    let points = cylinder.attributes.position.array;
    let normals = cylinder.attributes.normal.array;
    let faces : UseableFace[] = [];
    let positionedFaces : UseableFace[][] = [];
    for(let i = 0; i < triangles.length; i += 6) {
        let planePoints = new Map<string, THREE.Vector3>();
        let normalPoints : THREE.Vector3[] = [];
        for(let j = i; j < i + 6; j++) {
            let tempVector = new THREE.Vector3(points[triangles[j] * 3], points[triangles[j] * 3 + 1], points[triangles[j] * 3 + 2]);
            if (planePoints.has(tempVector.toArray().toString())) {
                planePoints.delete(tempVector.toArray().toString());
            } else {
                planePoints.set(tempVector.toArray().toString(), tempVector);
            }
            if (j < i + 3) {
                normalPoints.push(tempVector); //new THREE.Vector3(normals[triangles[j] * 3], normals[triangles[j] * 3 + 1], normals[triangles[j] * 3 + 2]));
            }
        }
        let centroid = new THREE.Vector3();
        for(let value of planePoints.values()) {
            centroid.add(value);
        }
        centroid.multiplyScalar(0.5);

        let normal = normalPoints[1].sub(normalPoints[0]).normalize();
        normal = normalPoints[2].sub(normalPoints[0]).normalize().cross(normal).multiplyScalar(2);
        // normal.applyAxisAngle(new THREE.Vector3(0, 0, 0), Math.PI / 2);
        // let normal = new THREE.Vector3();
        // normalPoints.map((point) => {normal.add(point);});
        // normal.multiplyScalar(1/3);


        faces.push(new UseableFace(centroid, normal, mesh, "menu"));
        if (faces.length % parameters.heightSegments == 0) {
            positionedFaces.push(faces);
            faces = [];
        }
    }
    positionedFaces = positionedFaces[0].map((col, i) => positionedFaces.map(row => row[i]));
    positionedFaces = positionedFaces.map((row) => row.reverse());
    return positionedFaces;
}