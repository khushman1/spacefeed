import * as THREE from "three";

export interface VisualAsset {
    id : string;
    enabled : boolean;
}
