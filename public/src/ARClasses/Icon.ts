import * as THREE from "three";
const TWEEN = require('es6-tween');

import { ARElement } from "./ARElement";
import { VisualAsset } from "./VisualAsset";
import { Region } from "./Region";
import * as _u from "../utils/util"
import { Vector3 } from "three";


let timeout = 100;

export class Icon {
    static params = {"pin":{"path": "./images/pin.png", "fadePath": "./images/pin_fade.png"},
                     "drag": {"path": "./images/drag.png", "fadePath": "./images/drag_fade.png"},
                     "tick": {"path": "./images/tick.png", "fadePath": "./images/tick_fade.png"},
                     "untick": {"path": "./images/untick.png", "fadePath": "./images/untick_fade.png"},
                    }

    id : string;
    enabled : boolean;
    filePath : string;
    asset : THREE.Mesh;
    assetLoaded : boolean = false;
    fadePath : string;
    fadeAsset : THREE.Mesh;
    fadeAssetLoaded : boolean = false;
    tween : any;
    reverseTween : any;
    visibleTween: any;
    reverseVisibleTween : any;
    parentRegion : Region;
    size : number = 0.6 / 7;
    type : Icon.TYPE;

    constructor(region : Region, type : Icon.TYPE) {
        this.parentRegion = region;
        let name : string = Object.keys(Icon.params)[type];
        this.type = type;
        let filePath : string = Icon.params[name]["path"];
        let fadeFilePath : string = Icon.params[name]["fadePath"];
        if(!("iconMaterial" in Icon.params[name])) {
            var { imageGeometry, imageMaterial } = _u.imageInit(Icon.params[name]["path"], this.size, () => {this.setAssetLoaded()});
            Icon.params[name]["iconMaterial"] = imageMaterial;
        } else {
            var imageGeometry = new THREE.PlaneBufferGeometry(this.size, this.size);
            var imageMaterial = Icon.params[name]["iconMaterial"].clone();
            this.assetLoaded = true;
        }
        imageMaterial.opacity = 0.0;
        this.asset = _u.createImage(imageGeometry, imageMaterial);
		this.asset.userData = {"parent": this};
        if (fadeFilePath !== undefined) {
            this.fadePath = fadeFilePath;
            if(!("fadeMaterial" in Icon.params[name])) {
                var { imageGeometry, imageMaterial } = _u.imageInit(Icon.params[name]["fadePath"], this.size, () => {this.setFadeAssetLoaded()});
                Icon.params[name]["fadeMaterial"] = imageMaterial;
            } else {
                var imageGeometry = new THREE.PlaneBufferGeometry(this.size, this.size);
                var imageMaterial = Icon.params[name]["fadeMaterial"].clone();
                this.fadeAssetLoaded = true;
            }
            imageMaterial.opacity = 0.0;
            imageMaterial.visible = false;
            this.fadeAsset = _u.createImage(imageGeometry, imageMaterial);
			this.fadeAsset.userData = {"parent": this};
            this.asset.add(this.fadeAsset);
            this.fadeAsset.position.z += 0.001;
            this.createTween();
        }
        this.filePath = filePath;
        this.fadeAssetLoaded = false;
    }

    dispose() {
        this.parentRegion.localObject.remove(this.asset, this.fadeAsset);
        this.asset.geometry.dispose();
        (this.asset.material as THREE.Material).dispose();
        this.fadeAsset.geometry.dispose();
        (this.fadeAsset.material as THREE.Material).dispose();
    }

    setAssetLoaded() {
        this.assetLoaded = true;
    }

    setFadeAssetLoaded() {
        this.fadeAssetLoaded = true;
    }

    setPosition(position : THREE.Vector3) {
        this.asset.position.set(position.x, position.y, position.z);
    }

    getRegion() {
        return this.parentRegion;
    }

    setRegion(region : Region) {
        this.parentRegion = region;
    }

    addToObject(scene : THREE.Object3D) {
        this.addAsset(scene);
    }

    resetAnimations() {
        (this.asset.material as THREE.Material).opacity = 0.0;
        this.stopTween();
        this.stopReverseTween();
    }

    startTween() {
        this.tween.start();
        this.tween.play();
        if(this.parentRegion) {
            this.parentRegion.startTween();
        }
    }

    startReverseTween() {
        this.reverseTween.start();
        this.reverseTween.play();
        if(this.parentRegion) {
            this.parentRegion.startReverseTween();
        }
    }

    stopTween() {
        if(this.tween.isPlaying()) {
            this.tween.stop();
        }
        if(this.parentRegion) {
            this.parentRegion.stopTween();
        }
    }

    stopReverseTween() {
        if(this.reverseTween.isPlaying()) {
            this.reverseTween.stop();
        }
        if(this.parentRegion) {
            this.parentRegion.stopReverseTween();
        }
    }

    createTween() {
        let opacity = 0.0;
        this.tween = new TWEEN.Tween({opacity: 0.0}).to({opacity: 1.0}, 1000)
            .on('start', () => {(this.fadeAsset.material as THREE.Material).visible = true;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let fadeMaterial : THREE.Material = this.fadeAsset.material as THREE.Material;
                let material : THREE.Material = this.asset.material as THREE.Material;
                material.opacity = 1.0 - opacity;
                fadeMaterial.opacity = opacity;
            });
            // .on('complete', () => {(this.asset.material as THREE.Material).visible = false;});
        this.reverseTween = new TWEEN.Tween({opacity: 1.0}).to({opacity: 0.0}, 1000)
            .on('start', () => {(this.asset.material as THREE.Material).visible = true;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let fadeMaterial : THREE.Material = this.fadeAsset.material as THREE.Material;
                let material : THREE.Material = this.asset.material as THREE.Material;
                material.opacity = 1.0 - opacity;
                fadeMaterial.opacity = opacity;
            });
            // .on('complete', () => {(this.fadeAsset.material as THREE.Material).visible = false;});
        this.visibleTween = new TWEEN.Tween({opacity: 0.0}).to({opacity: 1.0}, 200)
            .on('start', () => {(this.asset.material as THREE.Material).opacity = 0.0;})
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let material : THREE.Material = this.asset.material as THREE.Material;
                material.opacity = opacity;
            });
        this.reverseVisibleTween = new TWEEN.Tween({opacity: 1.0}).to({opacity: 0.0}, 200)
			.easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', ({opacity}) => {
                let material : THREE.Material = this.asset.material as THREE.Material;
                material.opacity = opacity;
            });
    }

    /*imageInit(filePath : string, size : number, changeFlag? : boolean) {
        let imageLoader : THREE.MaterialLoader = new THREE.MaterialLoader();
        let aspectRatio : number = 1;
        let currScale = 1;
        let imageGeometry : THREE.PlaneBufferGeometry = new THREE.PlaneBufferGeometry(size, size);
        let imageMaterial : any = new THREE.MeshBasicMaterial({
            map: imageLoader.load( filePath, (texture) => {
                if(changeFlag) {
                    this.fadeAsset.scale.set(1 / currScale, (texture.image.height / texture.image.width) / currScale, 1 / currScale);
                    this.asset.add(this.fadeAsset);
                    this.fadeAssetLoaded = true;
                    this.fadeAsset.position.z += 0.1;
                } else {
                    currScale = 0.5;
                    if(this.fadePath == undefined) {
                        currScale = 1;
                    }
                    this.asset.scale.set(currScale, currScale * texture.image.height / texture.image.width, currScale);
                }
            }),
            transparent: true,
            side: THREE.DoubleSide
        });
        return { imageGeometry, imageMaterial };
    }*/

    addAsset(scene) {
        if(this.assetLoaded) {
            scene.add(this.asset);
        } else {
            setTimeout(() => {this.addAsset(scene)}, timeout);
        }
    }

    addFadeAsset(scene) {
        if(this.fadeAssetLoaded) {
            scene.add(this.fadeAsset);
        } else {
            setTimeout(() => {this.addFadeAsset(scene)}, timeout);
        }
    }

    getSize() {
        let size : THREE.Vector2 = new THREE.Vector2(0, 0);
        let assetParams = (this.asset.geometry as THREE.PlaneBufferGeometry).parameters;
        let fadeAssetParams = (this.fadeAsset && (this.fadeAsset.geometry as THREE.PlaneBufferGeometry).parameters);
        size.x = Math.max(assetParams.width * this.asset.scale.x, this.fadeAsset && fadeAssetParams.width * this.fadeAsset.scale.x * this.asset.scale.x || 0);
        size.y = Math.max(assetParams.height * this.asset.scale.y, this.fadeAsset && fadeAssetParams.height * this.fadeAsset.scale.y * this.asset.scale.y || 0);
        return size;
    }

    scale(inp : THREE.Vector3) {
        this.asset.scale.copy(inp);
    }

    toggleVisible() {
        this.asset.visible = !this.asset.visible;
    }
}

export namespace Icon {
    export enum TYPE {
        Pin = 0,
        Drag,
        Tick,
        Untick
    }
}
