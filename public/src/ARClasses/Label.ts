import * as THREE from "three";

import { VisualAsset } from "./VisualAsset";

export class Label implements VisualAsset {
    id : string;
    enabled : boolean;
    asset : THREE.Object3D;
    assetLoaded : boolean;
    labelText : string;

    constructor(labelText : string, options : any) {
        this.labelText = labelText;
        this.createLabel(options);
    }

    createLabel(options : any) {
        this.asset = makeTextSprite(this.labelText, options);
        this.assetLoaded = true;
    }

    addToScene(scene) {
        console.log("lab", this);
        scene.add(this.asset);
    }
}


function makeTextSprite( message, parameters )
{
    if ( parameters === undefined ) parameters = {};

    var fontface = parameters.hasOwnProperty("fontface") ? 
        parameters["fontface"] : "Arial";

    var fontsize = parameters.hasOwnProperty("fontsize") ? 
        parameters["fontsize"] : 18;

    var borderThickness = parameters.hasOwnProperty("borderThickness") ? 
        parameters["borderThickness"] : 4;

    var borderColor = parameters.hasOwnProperty("borderColor") ?
        parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

    var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
        parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    context.font = "Bold " + fontsize + "px " + fontface;

    // get size data (height depends only on font size)
    var metrics = context.measureText( message );
    var textWidth = metrics.width;

    // background color
    context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
        + backgroundColor.b + "," + backgroundColor.a + ")";
    // border color
    context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
        + borderColor.b + "," + borderColor.a + ")";

    context.lineWidth = borderThickness;
    roundRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
    // 1.4 is extra height factor for text below baseline: g,j,p,q.

    // text color
    context.fillStyle = "rgba(0, 0, 0, 1.0)";

    context.fillText( message, borderThickness, fontsize + borderThickness);

    // canvas contents will be used for a texture
    let texture : THREE.Texture = new THREE.Texture(canvas); 
    texture.needsUpdate = true;
    interface SpriteParameters {
        map: THREE.Texture,
            useScreenCoordinates: boolean
    }
    let spriteParameters = {} as SpriteParameters;
    spriteParameters.map = texture;
    spriteParameters.useScreenCoordinates = false;

    var spriteMaterial : any = new THREE.SpriteMaterial( spriteParameters );
    var sprite = new THREE.Sprite( spriteMaterial );
    sprite.scale.set(20,10,0.5);
    return sprite;	
}

// function for drawing rounded rectangles
function roundRect(ctx, x, y, w, h, r) 
{
    ctx.beginPath();
    ctx.moveTo(x+r, y);
    ctx.lineTo(x+w-r, y);
    ctx.quadraticCurveTo(x+w, y, x+w, y+r);
    ctx.lineTo(x+w, y+h-r);
    ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
    ctx.lineTo(x+r, y+h);
    ctx.quadraticCurveTo(x, y+h, x, y+h-r);
    ctx.lineTo(x, y+r);
    ctx.quadraticCurveTo(x, y, x+r, y);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();   
}
