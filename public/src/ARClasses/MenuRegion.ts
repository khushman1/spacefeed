import * as THREE from "three";
const TWEEN = require('es6-tween');

import { Region } from "./Region";
import { WindowPlacementManager } from "../WindowPlacementManager";
import { Icon } from "./Icon";

export class MenuRegion extends Region {
    enabled : boolean;
    attachedRegion : Region;
    static parentCylinder : THREE.Mesh;
    tick : Icon;
    untick : Icon;

    constructor(scene : THREE.Scene, priority : number, assets? : any[]) {
        let newAssets = [];
        for(let asset of assets) {
            newAssets.push(asset.copy(Region.scene));
        }
        super(scene, priority, "menu", newAssets);
        this.createRegion();
        MenuRegion.parentCylinder.add(this.localObject);
        this.orient = true;
        this.localObject.scale.set(0.21, 0.21, 0.21);
    }

    addTicks() {
        this.tick = new Icon(this, Icon.TYPE.Tick);
        this.untick = new Icon(this, Icon.TYPE.Untick);
    }

    createPlane() {
        super.createPlane();

        this.tick.addToObject(this.localObject);
        this.untick.addToObject(this.localObject);
        this.untick.toggleVisible();

        let tempVector = new THREE.Vector3(3, 3, 3);
        this.tick.scale(tempVector);
        this.untick.scale(tempVector);

        tempVector.set(this.size.x - this.tick.getSize().x, this.size.y - this.tick.getSize().y, 0.05).multiplyScalar(0.4);
        this.tick.setPosition(tempVector);
        this.untick.setPosition(tempVector);

        this.tick.visibleTween.start();
        this.tick.visibleTween.play();
        this.untick.visibleTween.start();
        this.untick.visibleTween.play();
    }

    createRegion() {
        this.addTicks();
        super.createRegion();
    }

    attachRegion(region : Region) {
        this.attachedRegion = region;
    }

    static makeFromRegion(region : Region) {
        return new MenuRegion(Region.scene, region.priority, region.assets);
    }

    static attachMenu(parentCylinder : THREE.Mesh) {
        MenuRegion.parentCylinder = parentCylinder;
    }

    toggleTick() {
        this.tick.toggleVisible();
        this.untick.toggleVisible();
    }
}