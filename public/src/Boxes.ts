import * as THREE from "three";

let color : THREE.Color = new THREE.Color();

export class Boxes {
    numberOfBoxes : number;
    size : number;

    constructor(num : number) {
        this.numberOfBoxes = num;
        this.size = 20;
    }

    generateBoxes() : any[] {

        let boxGeometry : any = new THREE.BoxBufferGeometry(this.size, this.size, this.size);
        boxGeometry = boxGeometry.toNonIndexed(); // ensure each face has unique vertices
        let objects : any[] = [];

        let position : THREE.BufferAttribute = boxGeometry.attributes.position;
        let colors : any[] = [];

        for ( var i = 0, l = position.count; i < l; i ++ ) {
            color.setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
            colors.push( color.r, color.g, color.b );
        }

        boxGeometry.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );

        for (let i = 0; i < this.numberOfBoxes; i++) {
            var boxMaterial = new THREE.MeshPhongMaterial(
                { specular: 0xffffff, flatShading: true, vertexColors: true } );
            boxMaterial.color.setHSL( Math.random() * 0.2 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );

            let box : THREE.Mesh = new THREE.Mesh( boxGeometry, boxMaterial );
            box.position.x = Math.floor( Math.random() * 20 - 10 ) * 20;
            box.position.y = Math.floor( 10 );//Math.random() * 20 ) * 20 + 10;
            box.position.z = Math.floor( Math.random() * 20 - 10 ) * 20;
            //addImageToFaces(box, imageGeometry, imageMaterial);
            objects.push( box );

            console.log(box);
        }
        return objects;
    }
}
