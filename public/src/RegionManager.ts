import * as THREE from "three";

import { UseableFace } from "./ARClasses/UseableFace";
import { Region } from "./ARClasses/Region";
import { WindowPlacementManager } from "./WindowPlacementManager";
import { MenuRegion } from "./ARClasses/MenuRegion";

export class RegionManager {

    private static _instance : RegionManager = new RegionManager();

    private static regionMap : Map<string, Region>;
    private static menuRegionMap : Map<string, MenuRegion>;
    private static objects : any[];
    private static categoryIndex : Map<string, number>;
    private static raycastTargetScene : THREE.Scene;

    constructor() {
        if(RegionManager._instance) {
            return;
        }
        RegionManager._instance = this;
        RegionManager.regionMap = new Map();
        RegionManager.menuRegionMap = new Map();
        RegionManager.objects = [];
        RegionManager.categoryIndex = new Map<string, number>();
    }

    public static setRaycastScene(scene : THREE.Scene) {
        this.raycastTargetScene = scene;
    }

    public static getRaycastScene() {
        return this.raycastTargetScene;
    }

    private static addRegion(region : Region) {
        if(this.categoryIndex.has(region.category)) {
            region.priority = this.categoryIndex.get(region.category) + 1;
        } else {
            region.priority = 0;
        }
        this.regionMap.set(region.id, region);
        this.categoryIndex.set(region.category, region.priority);
        if (!this.menuRegionMap.has(region.id)) {
            this.menuRegionMap.set(region.id, MenuRegion.makeFromRegion(region));
        }
    }

    public static addRegions(regions : Region[]) {
        for(let region of regions) {
            this.addRegion(region);
        }
    }

    public static addRegionsFromAssets(scene, assets : any[], category? : string) {
        if(category == undefined) {
            category = "Unlisted";
        }
        for(let asset of assets) {
            let region = new Region(scene, 0, category, [asset]);
            this.addRegion(region);
        }
    }

    public static createAllRegions() {
        for(let [id, region] of this.regionMap) {
            region.recreatePlane();
        }
        WindowPlacementManager.addMarkers([...this.regionMap.values()]);
        console.log("RegionManager", this);
    }

    public static mergeRegions(region1 : Region, region2 : Region) {
        this.deleteRegion(region1);
        this.deleteRegion(region2);
        region1.appendAssets(region2.assets);
        region1.priority = Math.max(region1.priority, region2.priority, 0);
        region2.dispose();
        // WindowPlacementManager.deleteFaces([region1.currentSurface]);
        // region1.currentSurface.category = region1.id;
        // WindowPlacementManager.addFaces([region1.currentSurface]);
        region1.recreatePlane();
        this.addRegion(region1);
        region1.resetAnimations();
        region1.assignToFace(region1.currentSurface);
        // RegionManager.togglePin(region1);
        // WindowPlacementManager.addMarker(region1);
    }

    public static togglePin(region : Region) {
        if (region.currentSurface) {
            WindowPlacementManager.deleteMarker(region);
            WindowPlacementManager.deleteFaces([region.currentSurface]);
            region.category = (region.category == "Unlisted") ? region.id : "Unlisted"
            region.currentSurface.category = region.category;
            WindowPlacementManager.addFaces([region.currentSurface]);
            WindowPlacementManager.addMarker(region);
            region.togglePin();
        }
    }

    public static deleteRegion(region : Region) {
        this.raycastTargetScene.remove(region.localObject);
        this.regionMap.delete(region.id);
        WindowPlacementManager.deleteMarker(region);
    }

    public static getAllRegions() {
        return [...this.regionMap.values()];
    }

    public static getAllMenuRegions() {
        return [...this.menuRegionMap.values()];
    }

    public static getInstance() : RegionManager {
        return RegionManager._instance;
    }
}
