import * as THREE from "three";

function createImage(imageGeometry : THREE.PlaneBufferGeometry, imageMaterial : THREE.MeshBasicMaterial, position? : THREE.Vector3) {
    let imageMesh : THREE.Mesh = new THREE.Mesh(imageGeometry, imageMaterial);
    if (position == undefined) {
        position = new THREE.Vector3(0, 0, 0);
    }
    imageMesh.position.set(position.x, position.y, position.z);

    return imageMesh;
}

function createImageMaterial(filePath : string) {
    let imageLoader : THREE.TextureLoader = new THREE.TextureLoader();
    let imageMaterial : THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({
        map : imageLoader.load(filePath, (texture) => {}),
        transparent : true,
        side : THREE.DoubleSide
    });
    return imageMaterial;
}

function imageInit(filePath : string, size : number, callback? : Function) {
    let imageLoader : THREE.TextureLoader = new THREE.TextureLoader();
    let currScale = 1;
    let imageGeometry : THREE.PlaneBufferGeometry = new THREE.PlaneBufferGeometry(size, size);
    let imageMaterial : THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({
        map: imageLoader.load( filePath, (texture) => {
            imageGeometry.scale(currScale, currScale * texture.image.height / texture.image.width, currScale);
            if(callback != undefined) {
                callback();
            }
        }),
        transparent: true,
        side: THREE.DoubleSide
    });
    return { imageGeometry, imageMaterial };
}

export {createImage, createImageMaterial, imageInit};




function getCentroid9Array( pointArray : number [] ) : THREE.Vector3 {
    let sums : number [] = [ 0, 0, 0 ];
    for( let i = 0; i < 3; i++ ) {
        for( let j = 0; j < 3; j++ ) {
            sums[j] += pointArray[3 * i + j];
        }
    }
    return new THREE.Vector3(...sums.map(sum => sum / sums.length));
}




function combineSurfaces(position : number[], normals : number[]) {
    let megaMap : Map<string, Set<string>> = new Map();
    for(let i = 0; i < normals.length; i += 3) {
        let currNormal = String([normals[i], normals[i + 1], normals[i + 2]]);
        let pointSet : Set<string>;
        if(megaMap.has(currNormal)) {
            pointSet = megaMap.get(currNormal);
        } else {
            pointSet = new Set();
            megaMap.set(currNormal, pointSet);
        }
        for(let j = 0; j < 9; j+=3) {
            let currIndex : number = i * 3 + j;
            pointSet.add(String(position.slice(currIndex, currIndex + 3)));
        }
    }
    let centroids : THREE.Vector3[] = [];
    let newNormals : THREE.Vector3[] = [];
    megaMap.forEach((value, key) => {
        let currNormal = new THREE.Vector3(...(key.split(",").map(a => Number(a))));
        let pointArray : THREE.Vector3[] = [...value].map(a => new THREE.Vector3(...(a.split(",")).map(b => Number(b))));
        let centroid = getCentroid(pointArray);
        centroids.push(centroid);
        newNormals.push(currNormal);
    });
    return [centroids, newNormals];
}


function getCentroid(inpArray : THREE.Vector3[]) {
    let tempVec : THREE.Vector3 = new THREE.Vector3(0, 0, 0);
    for(let i = 0; i < inpArray.length; i++) {
        tempVec.x += inpArray[i].x / inpArray.length;
        tempVec.y += inpArray[i].y / inpArray.length;
        tempVec.z += inpArray[i].z / inpArray.length;
    }
    return tempVec;
}


function addFloor(scene : THREE.Scene) {
    // floor

    let floorGeometry : any = new THREE.PlaneBufferGeometry( 2000, 2000, 100, 100 );
    floorGeometry.rotateX( - Math.PI / 2 );

    // vertex displacement

    let position : any = floorGeometry.attributes.position;
    let vertex = new THREE.Vector3();

    for ( let i = 0, l = position.count; i < l; i ++ ) {
        vertex.fromBufferAttribute( position, i );

        vertex.x += Math.random() * 20 - 10;
        vertex.y += 0;//Math.random() * 2;
        vertex.z += Math.random() * 20 - 10;

        position.setXYZ( i, vertex.x, vertex.y, vertex.z );
    }

    floorGeometry = floorGeometry.toNonIndexed(); // ensure each face has unique vertices

    position = floorGeometry.attributes.position;
    let colors = [] as any;
    let color = new THREE.Color();

    for ( let i = 0, l = position.count; i < l; i ++ ) {
        color.setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
        colors.push( color.r, color.g, color.b );
    }

    floorGeometry.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
    let floorMaterial = new THREE.MeshBasicMaterial( { vertexColors: true } );
    let floor = new THREE.Mesh( floorGeometry, floorMaterial );
    scene.add( floor );

}