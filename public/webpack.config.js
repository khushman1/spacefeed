const path = require('path');
const ThreeWebpackPlugin = require('@wildpeaks/three-webpack-plugin');

module.exports = {
  entry: './src/main.ts',
  devtool: 'inline-source-map',
  mode: 'development',
  watch: false,
  watchOptions: {
      aggregateTimeout: 100,
      poll: 2500
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  devServer: {
      compress: true,
      disableHostCheck: true,
      https: true,
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
      //...
      new ThreeWebpackPlugin(),
  ]
};
